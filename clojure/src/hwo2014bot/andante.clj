(ns hwo2014bot.andante
	(:require [hwo2014bot.com :as c])
)  
  

(defmulti handle-msg :msgType)

(defmethod handle-msg "carPositions" [msg]
  {:msgType "throttle" :data 0.6})

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (c/read-message channel)]
    (log-msg msg)
    (c/send-message channel (handle-msg msg))
    (recur channel)))


(defn mayn [& [host port botname botkey]]
	(println "andante/mayn")
  (let [channel (c/connect-client-channel host (Integer/parseInt port))]
    (c/send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
